# Project 4: Brevet time calculator with Ajax

Author: Mason Jones
Contact: masonj@uoregon.edu
Use: First, choose your total distance, either 200, 300, 400, 600, or 1000. Next, enter the distance of your brevets. These can be any distance that is positive and under 120% of the total distance.

While under 200km, max speed = 34, min speed = 15

200km < n <= 400km, max speed = 32, min speed 15

400km < n <= 600km, max speed = 30, min speed 15

600km < n <= 1000km, max speed = 28, min speed 11.428
