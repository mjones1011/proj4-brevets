"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREVET_UNITS_MAX = [(0,34), (200,34), (300,32), (400,32), (600,30), (1000,28)]
BREVET_UNITS_MIN = [(0,15), (200,15), (300,15), (400,15), (600,15), (1000,11.428)]
ALL_TIMES = {200: 13.5, 300: 20.0, 400: 27.0, 600: 40.0, 1000: 75.0} #From ACP rules for riders Article 9

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    km = control_dist_km
    distance = brevet_dist_km
    begin = brevet_start_time
    
    if km > distance:
      km = distance

    racetime = 0
    i = 1
    while km > 0 and i < len(BREVET_UNITS_MAX):
      mindist = min(BREVET_UNITS_MAX[i][0] - BREVET_UNITS_MAX[i-1][0], km)
      racetime += mindist / BREVET_UNITS_MAX[i][1]
      km -= mindist
      i += 1
    
    hours = int(racetime)
    minutes = int((racetime % 1) * 60)
    begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
    return begin


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    km = control_dist_km
    distance = brevet_dist_km
    begin = brevet_start_time
    
    if km == 0:
      hours = int(1)
      minutes = int((1 % 1) * 60)
      begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
      return begin

    if km > distance:
      hours = int(ALL_TIMES[distance])
      minutes = int((ALL_TIMES[distance] % 1) * 60)
      begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
      return begin

    racetime = 0
    i = 1
    while km > 0 and i < len(BREVET_UNITS_MIN):
      mindist = min(BREVET_UNITS_MIN[i][0] - BREVET_UNITS_MIN[i-1][0], km)
      racetime += mindist / BREVET_UNITS_MIN[i][1]
      km -= mindist
      i += 1
    hours = int(racetime)
    minutes = int((racetime % 1) * 60)
    begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
    return begin
