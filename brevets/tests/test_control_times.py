import arrow

START = arrow.get("2000-01-01T00:00:00.000000-00:00")
START_TIME = START.isoformat()

def nowShift(days, hours, minutes):

    return START.shift(days=days, hours=hours, minutes=minutes).isoformat()

def test_200open():
    assert open_time(0, 200, START_TIME) == START_TIME
    assert open_time(200, 200, START_TIME) == nowShift(0, 5, 52)

def test_300open():
    assert open_time(10, 300, START_TIME) == nowShift(0, 0, 17)
    assert open_time(300, 300, START_TIME) == nowShift(0, 9, 0)

def test_600open():
    assert open_time(350, 600, START_TIME) == nowShift(0, 10, 34)
    assert open_time(600, 600, START_TIME) == nowShift(0, 18, 47)

def test_200close():
    assert close_time(0, 200, START_TIME) == nowShift(0, 1, 0)
    assert close_time(200, 200, START_TIME) == nowShift(0, 13, 20)

def test_300close():
    assert close_time(10, 300, START_TIME) == nowShift(0, 0, 40)
    assert close_time(300, 300, START_TIME) == nowShift(0, 20, 0)

def test_600close():
    assert close_time(350, 600, START_TIME) == nowShift(0, 23, 19)
    assert close_time(600, 600, START_TIME) == nowShift(1, 16, 0)

"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREVET_UNITS_MAX = [(0,34), (200,34), (300,32), (400,32), (600,30), (1000,28)]
BREVET_UNITS_MIN = [(0,15), (200,15), (300,15), (400,15), (600,15), (1000,11.428)]
ALL_TIMES = {200: 13.5, 300: 20.0, 400: 27.0, 600: 40.0, 1000: 75.0} #From ACP rules for riders Article 9

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    km = control_dist_km
    distance = brevet_dist_km
    begin = brevet_start_time
    
    if km > distance:
      km = distance

    racetime = 0
    i = 1
    while km > 0 and i < len(BREVET_UNITS_MAX):
      mindist = min(BREVET_UNITS_MAX[i][0] - BREVET_UNITS_MAX[i-1][0], km)
      racetime += mindist / BREVET_UNITS_MAX[i][1]
      km -= mindist
      i += 1
    
    hours = int(racetime)
    minutes = int((racetime % 1) * 60)
    begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
    return begin


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    km = control_dist_km
    distance = brevet_dist_km
    begin = brevet_start_time
    
    if km == 0:
      hours = int(1)
      minutes = int((1 % 1) * 60)
      begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
      return begin

    if km > distance:
      hours = int(ALL_TIMES[distance])
      minutes = int((ALL_TIMES[distance] % 1) * 60)
      begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
      return begin

    racetime = 0
    i = 1
    while km > 0 and i < len(BREVET_UNITS_MIN):
      mindist = min(BREVET_UNITS_MIN[i][0] - BREVET_UNITS_MIN[i-1][0], km)
      racetime += mindist / BREVET_UNITS_MIN[i][1]
      km -= mindist
      i += 1
    hours = int(racetime)
    minutes = int((racetime % 1) * 60)
    begin = arrow.get(begin).shift(hours=hours, minutes=minutes).isoformat()
    return begin
